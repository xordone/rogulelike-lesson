# The First RogueLike 
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE.md) &nbsp;
[![Donate](https://img.shields.io/badge/donate-For_Coffee-green.svg)](https://rocketbank.ru/dead_PXL) &nbsp;


I write very bad English, and so дальше я буду писать на Русском.
### Базовая часть
За основу взят фундаментальный учебник доступный [тут](http://rogueliketutorials.com/tutorials/tcod/)

### Запуск
1. run `pip install pipenv`
2. run `pipenv shell`
3. run `pipenv install`
4. run `python engine.py`

## План:
- [x] Генератор карт
- [x] Область видимости
- [x] Вражеские обьекты
  - Раскурить алгоритм A*
- [x] Атака и урон
- [x] ЮИ
- [ ] Инвентарь
- [ ] Прицел
- [ ] Сохранения
- [ ] Механика
- [ ] Сеттинг
- [ ] Повышение ЧСВ
## Комьюнити(Как будто оно будет)
Рад любому фидбеку
gitlab - ну тут все понятно  
telegram - @Xordone
