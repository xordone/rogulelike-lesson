class Tile:
    """
    Тайл на карте, может быть заблокирован, может брокировать область видимости
    """

    def __init__(self, blocked, block_sight=None):
        self.explored = False
        self.blocked = blocked
        # По умолчанию если тайл заблокирован, то он так же блокирует и область видимости
        if block_sight is None:
            block_sight = blocked

        self.block_sight = block_sight
